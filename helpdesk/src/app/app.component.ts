import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LiveChatWidgetApiModel } from '@livechat/angular-widget';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Helpdesk-material';
  liveChatApi: LiveChatWidgetApiModel

  constructor(public router: Router) {
    
  }

  onChatLoaded(api: LiveChatWidgetApiModel) {
    this.liveChatApi = api;
  }
}
