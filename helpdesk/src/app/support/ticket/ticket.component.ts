import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from './dialog/dialog.component';

@Component({
    selector: 'app-ticket',
    templateUrl: './ticket.component.html',
    styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit {

    controlSubject = new FormControl('', [Validators.required]);
    controlOption = new FormControl('', [Validators.required]);
    controlText = new FormControl('', [Validators.required]);

    regiForm: FormGroup;
    FirstName: string = '';
    LastName: string = '';
    Address: string = '';
    DOB: Date = null;
    Gender: string = '';
    Email: string = '';
    Title: string = '';
    IsAccepted: number = 0;
    TitleModal: string;

    constructor(public dialog: MatDialog, private fb: FormBuilder, public httpClient: HttpClient) {

        // To initialize FormGroup  
        this.regiForm = fb.group({
            'FirstName': [null, Validators.required],
            'LastName': [null, Validators.required],
            'Address': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(500)])],
            'Priority': [null, Validators.required],
            'Email': [null, Validators.compose([Validators.required, Validators.email])],
            'Title': [null, Validators.required],
            'Description': [null, Validators.required],
            'IsAccepted': [null]
        });

    }

    // On Change event of Toggle Button  
    onChange(event: any) {
        if (event.checked == true) {
            this.IsAccepted = 1;
        } else {
            this.IsAccepted = 0;
        }
    }

    // Executed When Form Is Submitted  
    onFormSubmit(form: NgForm) {
        this.httpClient.post('http://voipcorp.tk:4321/submitForm', form).subscribe((res) => {
            console.log(res);
        });
        const dialogRef = this.dialog.open(DialogComponent, {
            width: '25%',
            data: { dataTitle: this.Title }
        });
    }

    ngOnInit(): void {
    }

}
