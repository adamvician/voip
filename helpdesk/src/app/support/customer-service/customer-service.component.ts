import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customer-service',
  templateUrl: './customer-service.component.html',
  styleUrls: ['./customer-service.component.css']
})
export class CustomerServiceComponent implements OnInit {

  breakpoint: any;

  constructor() { }

  ngOnInit(): void {
    this.breakpoint = (window.innerWidth <= 1580) ? 1 : 2;
  }

  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 1580) ? 1 : 2;
  }

}
