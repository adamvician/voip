import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SupportComponent } from './support/support.component';
import { TicketComponent } from './support/ticket/ticket.component';
import { CustomerServiceComponent } from './support/customer-service/customer-service.component';
import { LivechatComponent } from './support/livechat/livechat.component';
import { FaqComponent } from './faq/faq.component';
import { ForumComponent } from './forum/forum.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'support',
    component: SupportComponent
  },
  {
    path: 'support/ticket',
    component: TicketComponent
  },
  {
    path: 'support/customer-service',
    component: CustomerServiceComponent
  },
  {
    path: 'support/livechat',
    component: LivechatComponent
  },
  {
    path: 'faq',
    component: FaqComponent
  },
  {
    path: 'forum',
    component: ForumComponent
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
