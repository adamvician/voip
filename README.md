# VoIP System

This is a complete preconfigured voip solution with monitoring and support page all in one in docker.

## Whats inside:

- asterisk
- prometheus
- grafana
- node-exporter
- cadvisor
- custom angular helpdesk web app

# Instalation

Make sure to run this docker file on linux server, otherwise it will not work. There was problem with networking and only solution was to set network_mode to host on asterisk container.

Start with

```sh
docker-compose up -d
```

Stop with

```sh
docker-compose down
```
