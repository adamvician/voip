import express, { Express } from 'express'
import nodemailer from 'nodemailer'
import { google } from 'googleapis';

type TForm = {
    FirstName: string,
    LastName: string,
    Address: string,
    Priority: string,
    Email: string,
    Title: string,
    Description: string,
    IsAccepted: string
}

const myOAuth2Client = new google.auth.OAuth2(
    "716708086522-pm7pr8tjd1g0n6qeuc5e5pd88fq8ctoj.apps.googleusercontent.com",
    "bermgNQqN_hpFUIxaMI1gDMF",
    "https://developers.google.com/oauthplayground"
)

myOAuth2Client.setCredentials({
    refresh_token: "1//04n2tPmj-C08kCgYIARAAGAQSNwF-L9Irull_AtJa-2pkAurUTRlKMy6pS9lYwnGaiAw1UidlPoc9B5pn1jmkuUuTV3K_8lUOARA"
});


export const attachAPI = (app: Express) => {

    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    })

    app.use(express.json());

    app.post('/submitForm', async (req, res) => {
        const body = <TForm>req.body;

        const token = await myOAuth2Client.getAccessToken()

        if (!token.token) {
            return
        }

        const transport = nodemailer.createTransport({
            service: "gmail",
            auth: {
                type: "OAuth2",
                user: "voipcorptk@gmail.com", //your gmail account you used to set the project up in google cloud console"
                clientId: "716708086522-pm7pr8tjd1g0n6qeuc5e5pd88fq8ctoj.apps.googleusercontent.com",
                clientSecret: "bermgNQqN_hpFUIxaMI1gDMF",
                refreshToken: "1//04n2tPmj-C08kCgYIARAAGAQSNwF-L9Irull_AtJa-2pkAurUTRlKMy6pS9lYwnGaiAw1UidlPoc9B5pn1jmkuUuTV3K_8lUOARA",
                accessToken: token.token //access token variable we defined earlier
            }
        });

        const from = `"${body.FirstName} ${body.LastName}" <${body.Email}>`

        transport.sendMail({
            from: from, // sender address
            to: "voipcorptk@gmail.com", // list of receivers
            subject: body.Title, // Subject line
            text: `
            From: ${from} 
            Priority: ${body.Priority}
            Address: ${body.Address}
            
            Message:
            ${body.Description}
            `, // plain text body
        }).then((info) => {
            console.log("Message sent:", info.messageId);
            res.send("Sent!");
        }).catch(err => {
            res.send("Fail!");
        })

    })

}