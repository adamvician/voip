import express, { Express } from 'express'
import { API_PORT, API_URL } from '../config';
import { attachAPI } from './api';
import cors from 'cors'

export function prepareServer(): Express {
    const app = express();

    attachAPI(app);

    return app;
}

export function startServer() {
    const server = prepareServer();
    server.use(cors())
    server.listen(API_PORT, () => {
        console.log('Server listening on:', API_URL);
    });
}
