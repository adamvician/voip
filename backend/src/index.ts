import { startServer } from "./server";

const start = async (): Promise<void> => {
    await startServer()
};

try {
    start();
} catch (err) {
    console.log(err);
}