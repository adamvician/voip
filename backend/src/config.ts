export const API_PORT = process.env.API_PORT || "4321";
export const API_URL = process.env.API_URL || "http://localhost:4321";